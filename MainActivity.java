package com.example.mongps;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity {

    // Un objet qui donne accès à la localisation
    private FusedLocationProviderClient fusedLocationClient;
    // Un objet qui gère les mises à jour de position
    private LocationCallback locationCallback;
    // Un objet qui envoie des demandes de position
    private final LocationRequest locationRequest = LocationRequest.create();

    Button btnPosition;
    TextView tvVille;
    TextView tvLatitude;
    TextView tvLongitude;

    boolean positionAutorisee = false;
    boolean requestingLocationUpdates = false;

    /** Cycle de vie : quand l'activité est créée */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Récupération des éléments graphiques
        btnPosition = findViewById(R.id.btn_position);
        tvVille = findViewById(R.id.tv_ville);
        tvLatitude = findViewById(R.id.tv_lat);
        tvLongitude = findViewById(R.id.tv_long);

        // Instanciation du client
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        // Configuration des demandes de position
        locationRequest.setInterval (10000); // Toutes les 10 secondes
        locationRequest.setFastestInterval (5000); // Au plus rapide 5 secondes
        locationRequest.setPriority (LocationRequest.PRIORITY_HIGH_ACCURACY); // Précision élevée

        // Gestion des réponses aux demandes de position
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) { return; }
                for (Location location : locationResult.getLocations()) {
                    // Mise à jour de l'interface avec les données de position
                    majInterfaceAvecPosition(location);
                }
            }
        };

        // Listener de clic sur le bouton
        btnPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Que faire quand on clique sur le bouton ?
                // Demander la position GPS et récupérer les coordonnées
                recupererDernierePosition();
            }
        });
    }

    /** Cycle de vie : quand l'activité est mise en pause */
    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    /** Arrêter la mise à jour en continu de la position */
    private void stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback);
        requestingLocationUpdates = false;
    }

    /** Cycle de vie : quand l'activité reprend */
    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    /**
     * Démarrer la mise à jour en continu de la position
     */
    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        if (positionAutorisee) {
            fusedLocationClient.requestLocationUpdates(locationRequest,
                    locationCallback,
                    Looper.getMainLooper());
        }
    }

    @SuppressLint("MissingPermission")
    private void recupererDernierePosition() {
        verifierPermissionLocalisation();
        // Récupérer la position
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    // Afficher la position
                    majInterfaceAvecPosition(location);
                }
            }
        });
    }

    /**
     * Met à jour l'écran avec les informations de position.
     *
     * @param location l'objet de position
     */
    private void majInterfaceAvecPosition(Location location) {
        if (location != null) {
            String latitude = String.valueOf(location.getLatitude());
            String longitude = String.valueOf(location.getLongitude());
            tvLatitude.setText(latitude);
            tvLongitude.setText(longitude);
        }
    }

    protected void createLocationRequest () {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @SuppressLint("MissingPermission")
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                verifierPermissionLocalisation();
                fusedLocationClient.requestLocationUpdates(locationRequest,
                        locationCallback,
                        Looper.getMainLooper());
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        // resolvable.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                    } catch (Exception sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }

    private void verifierPermissionLocalisation() {
        // Vérifier qu'on a le droit de le faire
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Demander la permission

            // Les permissions à demander
            String[] permissionsToAsk = new String[] {
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION};
            // On effectue la demande
            requestPermissions(permissionsToAsk, 0);
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Cas demande permission de la POSITION
        if (requestCode == 0) {
            gestionResultatPermissionPosition(grantResults);
        }
        // On imagine le cas où on doit gérer une autre demande de permission
        if (requestCode == 1) {
            // Par exemple, une demande d'accès aux contacts...
            // gestionResultatPermissionContacts(grantResults);
        }
    }

    private void gestionResultatPermissionPosition(@NonNull int... grantResults) {
        if (grantResults.length == 0) {
            return;
        }
        if (grantResults.length == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                positionAutorisee = true;
            }
        }
        if (grantResults.length == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    || grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                positionAutorisee = true;
            }
        }
    }
}